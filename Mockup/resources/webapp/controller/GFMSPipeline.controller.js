sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/json/JSONModel",
	"sap/m/Dialog",
	"sap/m/Table",
	"sap/m/Button",
	"sap/m/Column",
	"sap/m/Label",
	"sap/ui/core/Fragment",
	"sap/ui/table/Column",
	"sap/ui/table/Table",
	"sap/ui/core/syncStyleClass",
	"sap/ui/model/odata/ODataMetadata"
], function (Controller, MessageToast, JSONModel, Dialog, Table, Button, Column, Label, Fragment ) {
	"use strict";

	var iTimeoutId;
	var oModel;
	var oView;
	var proc;
	
	return Controller.extend("sap.suite.ui.commons.demo.tutorial.controller.GFMSPipeline", {
		onInit: function () {
			oView = this.getView();
			this.onOpenDialog();
			proc = this.byId("processflow1");
			proc.setZoomLevel("Three");
			var oTable = this.byId("hidden");
			oTable.setMinAutoRowCount(29);
			oTable.setThreshold(50);
			oModel = new sap.ui.model.odata.v2.ODataModel({serviceUrl:"/xsodata/dynamicDemo.xsodata",serviceUrlParams:{top:"29"}});
			this.getView().setModel(oModel);
			
			function fnLoadMetadata(){
				try {
					oTable.setModel(oModel);
					var oMeta = oModel.getServiceMetadata();
					for (var i = 0; i < oMeta.dataServices.schema[0].entityType[4].property.length; i++){
						var property = oMeta.dataServices.schema[0].entityType[4].property[i];
						oTable.addColumn(new sap.ui.table.Column({ label: property.name, template: property.name}));
					}
					oTable.bindRows({path: "/GFMSE", length:"29"});
				} catch (e) {
					console.log(e.toString());
				}
			}
			oModel.attachMetadataLoaded(oModel, function() {
				fnLoadMetadata();
			});
		},
		
		onOpenDialog: function() {
			if (!this.byId("busyDialog")) {
				Fragment.load({
					id: oView.getId(),
					name: "sap.suite.ui.commons.demo.tutorial.fragment.BusyDialog",
					controller: this
				}).then(function (oDialog) {
					this.getView().addDependent(oDialog);
				}.bind(this));
			}
			this.byId("busyDialog").open();
			this.holdDialogUntilLoaded();
			
		},
		
		holdDialogUntilLoaded: function () {
			var time = 100;
			function recWaitForData() {
				//console.log("check data");
				if(typeof (oModel.oData["GFMSE(0)"])!=="undefined"){
					//console.log("data checked");
					proc.optimizeLayout();
					oView.byId("busyDialog").close();
					clearInterval(iTimeoutId);
				}
			}
			iTimeoutId = setInterval(recWaitForData, time);
		},
		
		onNavButtonPressed: function () {
			this.getOwnerComponent().getRouter().navTo("home");
		},
		
		showDynamicInfoBox: function (sTitle) {
			var list = this.byId("DemoList");
				// create dialog lazily
				if (!this.byId("gfmsInfoBox")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "sap.suite.ui.commons.demo.tutorial.fragment.GFMSInfoBox",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
				list = this.byId("DemoList");
				this.byId("linksbutton").setVisible(true);
			} else {
				this.byId("gfmsInfoBox").open();
			}
			var index = 0;
			
			for(var l = 0;typeof (oModel.oData[["GFMSE(",(l),")"].join("")])!=="undefined";l++){
				if(oModel.oData[["GFMSE(",l,")"].join("")].ENVNAME === sTitle){
					index = l ;
				}
			}
			var strind = ["GFMSE(",index,")"].join("");
			list.removeAllItems();
			var headers = ["Environment Name", "Description", "Owning Group/POC", "Software Release", "RFMS Data Source", "GFMS Data Source",
			"Data From Production", "Front End URL", "Alternate Front End URL", "Alternate Header URL", "LUN", "Oracle DB", "Oracle CPU",
			"Tuxedo", "WebLogic", "WebMethods", "Free Space", "Filer", "DB Drives", "DB Server", "App Server", "Tux Server", "WL Server",
			"Page Last Manually Updated", "Page Last Manually Updated By", "Unique Configuration", "", "Momentum URL", "CCRC URL",
			"WebLogic Admin URL", "BIRT Admin URL", "JackRabbit Admin URL", "CCRC Admin URL", "Workflow WM Admin URL", "ESB WM Admin URL",
			"CCRC WM Admin URL", "CoreIDP URL", "CoreIDP Admin URL"];
			for(var k = 1; k < oModel.oMetadata.mEntitySets.GFMSE.__entityType.property.length - 11; k++){
				var prop = oModel.oMetadata.mEntitySets.GFMSE.__entityType.property[k].name;
				var txt = oModel.oData[strind][prop];
				if(txt !== null){
					list.addItem(new sap.m.ObjectListItem({title: headers[k-1] + ": " + txt}));
				}
			}
			this.byId("gfmsInfoBox").setTitle(sTitle);
			var aModel = new JSONModel("model/data/GFMSLinks.json");
			this.byId("linksList").setModel(aModel);
			this.byId("linksList").bindItems("/links/D1-GFMSDEV3",new sap.m.CustomListItem().addContent(new sap.m.Link({text: "{link}"})));
		},
		
		showDynamicInfoBoxProc: function (oEvent) {
			var context = oEvent.getParameters().getProperty("title");
			if(context !== "LD"){
				this.showDynamicInfoBox(context);
			}
		},
		
		rowDynamicBox: function(oEvent) {
			this.showDynamicInfoBox(oEvent.getSource().getText());
		},
		
		showEnvList: function () {
			var tab = this.byId("environmentListTable");
			var eModel = new sap.ui.model.odata.v2.ODataModel("/xsodata/dynamicDemo.xsodata", true);
			
			function fnLoadMetadata(){
				try {
					tab.setModel(eModel);
					var eMeta = eModel.getServiceMetadata();
					//first column setup is a special case, because we need to set up a clickable link. 
					var property = eMeta.dataServices.schema[0].entityType[3].property[1];
					tab.addColumn(new sap.ui.table.Column({
						label: property.name,
						template: new sap.m.Link({press:function(oEvent) {
							oView.getController().rowDynamicBox(oEvent);
						}}).bindProperty("text",property.name),
						resizable: false
					}));
					//now that we've set up our first column, 
					for (var i = 2; i < eMeta.dataServices.schema[0].entityType[3].property.length; i++){
						property = eMeta.dataServices.schema[0].entityType[3].property[i];
						tab.addColumn(new sap.ui.table.Column({ label: property.name, template: property.name, resizable: false}));
					}
					tab.bindRows("/GEList");
				} catch (e) {
					console.log(e.toString());	
				}
			}
			// create dialog lazily
			if (!this.byId("environmentList")) {
				// load asynchronous XML fragment
				Fragment.load({
					id: oView.getId(),
					name: "sap.suite.ui.commons.demo.tutorial.fragment.EnvironmentList",
					controller: this
				}).then(function (oDialog) {
					// connect dialog to the root view of this component (models, lifecycle)
					oView.addDependent(oDialog);
					oDialog.open();
				});
				tab = this.byId("environmentListTable");
				/*
				tab.attachCellClick(tab, function(oEvent) {
					var oCont = oView.getController();
					oCont.rowDynamicBox(oEvent);
				});*/
				eModel.attachMetadataLoaded(eModel, function() {
					fnLoadMetadata();
				});
			} else {
				this.byId("environmentList").open();
			}
			
		},
		

		onCloseEnvList: function () {
			this.byId("environmentList").close();
		},

		

		onCloseInfoBox: function () {
			this.byId("linksbutton").setPressed(false);
			this.onShowLinks();
			this.byId("gfmsInfoBox").close();
		},
		/**
		 * Handles the press event on a process flow node.
		 *
		 * @param {sap.ui.base.Event} oEvent The press event object
		 */
		onNodePressed: function (oEvent) {
			var sItemTitle = oEvent.getParameters().getTitle();
			MessageToast.show(this.getResourceBundle().getText("processFlowNodeClickedMessage", [sItemTitle]));
		},

		/**
		 * Getter for the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Calculates difference between expected and actual values
		 * @param {float} fFirstValue Expected value
		 * @param {float} fSecondValue Actual value
		 * @returns {number} Textual representation of delta between two given values with specifier measurement unit
		 */
		getValuesDelta: function (fFirstValue, fSecondValue) {
			return fSecondValue - fFirstValue;
		},
		onShowLinks: function(){
			var linksOn = this.byId("linksbutton").getPressed();
			
			this.byId("listPanel").setVisible(!linksOn);
			this.byId("stackPanel").setVisible(!linksOn);
			this.byId("linksList").setVisible(linksOn);
			//console.log("congrats, you pressed it");
		}
	});
});