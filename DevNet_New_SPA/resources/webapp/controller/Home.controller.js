sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
	"use strict";

	return Controller.extend("devnet-neo.DevNet_New_SPA.controller.Home", {
		onInit: function () {
			//alert("Initialize Application");
			var listOfLinks = this.getView().byId("listOfLinks");
			listOfLinks.setVisible(false);

			/* Placeholder data. This is supposed to be the list of
			 * links from the old DevNet.
			 * TODO: Probably don't want to store all this in here.
			 * Maybe pull it from a database
			 */
			var data = {
				links: [{
					text: "CGI Website",
					url: "https://www.cgi.com"
				}, {
					text: "HANA Web IDE",
					url: "https://ip-172-31-68-48.ec2.internal:53075/"
				}, {
					text: "Probably going to change this to Dynamic Side Content",
					url: "https://experience.sap.com/fiori-design-web/dynamic-side-content/"
				}]
			};

			var model = new JSONModel(data);
			//console.log(model.getJSON());
			listOfLinks.setModel(model);
			listOfLinks.bindAggregation("items", {
				path: "/links",
				template: new sap.m.ColumnListItem({
					cells: new sap.m.Link({
						text: "{text}",
						href: "{url}"
					})

				})
			});

		},

		toggleListOfLinks: function () {
			var listOfLinks = this.getView().byId("listOfLinks");
			var toggleListButton = this.getView().byId("showHideMenu");

			//if the toggle button is pressed, we show the thing. if it is unpressed, we hide it
			listOfLinks.setVisible(toggleListButton.getPressed());
		}

	});
});